#!/bin/bash

# This script is not intended to be executed blindly. It's only a walkthrough
# of the commands run to setup an instance of Katie on an EC2 box.
# The following line is a safety to prevent it from being run.
exit 0

# The walk-through beings here, after logging in to the EC2 box.
cd $HOME
git clone https://gitlab.com/matt.belhorn/katie-docker-compose.git katie

# Install docker
sudo apt-get update
sudo apt-get install docker docker-compose docker-containerd docker-registry

sudo usermod -aG docker "$(whoami)"
# Must log-out and back in to absorb group change

# Restart docker or enable it if not already running.
sudo systemctl enable --now docker
sudo systemctl restart docker

# Generate a personal access token and use it to reach the katie container image
# while it is not hosted publicly on dockerhub. Token must have the `read_registry` capability.
# https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html
mkdir -p $HOME/.docker
touch $HOME/.docker/config.json
chmod go= $HOME/.docker/config.json

# The credential can be kept in a proper credential store:
if false; then
  sudo apt-get install pass
  gpg --full-generate-key
  pass init
  wget "https://github.com/docker/docker-credential-helpers/releases/download/v0.6.3/docker-credential-pass-v0.6.3-amd64.tar.gz"
  tar xvzf "docker-credential-pass-v0.6.3-amd64.tar.gz"
  chmod a+x docker-credential-pass
  mkdir -p $HOME/.local/bin
  mv docker-credential-pass $HOME/.local/bin
  export PATH="$PATH:$HOME/.local/bin"
  rm "docker-credential-pass-v0.6.3-amd64.tar.gz"
  echo '{ credsStore": "pass" }' > $HOME/.docker/config.json
  pass insert docker-credential-helpers/docker-pass-initialized-check
else
  touch $HOME/katie/gitlab-access-token.key
  chmod go= $HOME/katie/gitlab-access-token.key
  echo "my-super-secret-gitlab-access-token" > $HOME/katie/gitlab-access-token.key
fi

docker login registry.gitlab.com -u matt.belhorn --password-stdin < ./gitlab-access-token.key
docker pull registry.gitlab.com/kadc/katie/preview/katie:latest

cd $HOME/katie
touch .env
chmod go= .env
# Edit .env to meet your needs. See .env.sample for an annotated example.
docker-compose up --no-start
docker-compose up -d

docker exec -it katie /bin/sh
./manage.py makemigrations
./manage.py migrate
./manage.py collectstatic
./manage.py initialize_katie
./manage.py createsuperuser
# exit container ^d

# Monitor the logs for errors.
docker-compose logs -f
