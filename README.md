# Katie Docker-Compose

This project demonstrates how to setup an instance of
[Katie](https://gitlab.com/kadc/katie.git) on a hosting platform like an AWS
EC2 instance using docker-compose. See `setup.sh` in this repo for the series
of commands used to stand up an EC2 box hosting Katie.

This is not the only way to stand up an instance of the app. Anything that
works for an ordinary django app/project should work.

# Prerequisites

- A Twilio account and phone number
- A hosting provider like an AWS EC2 box. The size and resources available
  depend on the projected volume of users and clients you anticipate supporting.
- A domain name for which LetsEncrypt will generate certificates.
- DNS records pointing a fully-qualified domain name to your hosting provider's
  server.
- An email account which permits outgoing mail through SMTP for the app to send
  mail if you wish to configure it to do so.

# Applying these Settings

Following `setup.sh`, you will need to create a `.env` file for docker-compose
to ingest the server and application configuration settings. See `.env.sample`
as an example. The parameters in `.env` are sensitive secrets. Please do not
share the contents of your `.env` file with anyone.

# Generating a `KATIE_SECRET_KEY`

This envar is an alias for the standard Django `SECRET_KEY` when using Katie's
stock `settings.py` which allows for configuration to be done through a `.env`
file. The value of this variable must be long, unique, and kept secret. Use the
output of the following command to generate a value for it. 

```
python -c 'import random; result = "".join([random.choice("abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)") for i in range(50)]); print(result)'
```

